clear all


%% -----------------------------------------------------------
f0 = 0.2;

day = {'154','191'};
for di = 1:length(day)
    system('rm list.log')
    system(['find ~/DATA/Homestake_COH/ -type f -name *' day{di} '.mat >> list.log']);

    fid = fopen('list.log');
    filelist = textscan(fid,'%s');
    fclose(fid);

    filelist = filelist{:,:};
    
    takeout1 = strfind(filelist,'SHL');
    takeout2 = strfind(filelist,'DEAD');
    iito = [];
    for k = 1:length(filelist)
        if ~isempty(takeout1{k}) || ~isempty(takeout2{k})
            iito(length(iito)+1) = k;
        end
    end
    filelist(iito) = [];
    
    npairs = length(filelist);

    coh2P = zeros(1,npairs);
    distance = zeros(1,npairs);
    stationA = {};
    stationB = {};
    for k = 1:npairs
        datacont = load(filelist{k});

        [~, fi] = min(abs(f0-datacont.data.ff));

        daycoh2P = datacont.data.coherence_real(fi,:);
        coh2P(k) = mean(daycoh2P);

        stationA{k} = datacont.channel1.station;
        stationB{k} = datacont.channel2.station;

        loc1 = datacont.channel1.location;
        loc2 = datacont.channel2.location;
        distance(k) = norm(loc1(1:2)-loc2(1:2));
    end

    figure(1)
    clf
    scatter(distance,coh2P,30,'k','filled')
    for k = 1:npairs
        text(distance(k),coh2P(k),[stationA{k} ' / ' stationB{k}],'FontSize',5)
    end
    xlabel('Horizontal Distance [m]')
    ylabel(['Coherence (real part) at ' num2str(f0) 'Hz'])
    set(gca,'ylim',[0.5 1.2])
    saveas(gcf,['Homestake_COH_' day{di} '_text.fig'])
    
    figure(2)
    clf
    scatter(distance,coh2P,30,'k','filled')
    xlabel('Horizontal Distance [m]')
    ylabel(['Coherence (real part) at ' num2str(f0) 'Hz'])
    set(gca,'ylim',[0.5 1.2])
    saveas(gcf,['Homestake_COH_' day{di} '.fig'])
end