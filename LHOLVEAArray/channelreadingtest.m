addpath(genpath('/usr/share/ligotools/matlab/'));

gps0 = tconvert('2016/12/01 00:00:00');

%% array data, tiltmeter
system(char(['gw_data_find -o H -t H1_R -u file' ...
    ' -s ' num2str(gps0) ' -e ' num2str(gps0+3600) ' -u file --lal-cache > frames_array.log']));

fid = fopen('frames_array.log'); 
columns = textscan(fid,'%s %s %s %s %s');
files_array = char(columns{5});
files_array = files_array(:,17:end);

N = length(files_array(:,1));
fs = 64;    %sampling frequency after decimating
T = 64;     %length of frame file in seconds

%% top mass
disp(['Reading top mass OSEM data from ' files_array(1,:)])
try
    ss = frgetvect(files_array(1,:),char('H1:SUS-ITMX_M0_DAMP_L_IN1_DQ'),gps0,T);
    size(ss)
catch exception
    disp(exception.identifier)
end