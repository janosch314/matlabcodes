%% read O2 data; tiltmeter moved to ITMY on June 20, 2017
% check data: https://ldas-jobs.ligo-la.caltech.edu/~detchar/summary/day/20161201/

gps0 = tconvert('2016/12/01 00:00:00'); %start of O2: Nov 30, 2016
% sensitivity break between Dec 23, 2016, and Jan 4, 2017

for k = 57:73 %[1:22 36:73] %until Feb 12, 2017
    gps = gps0+(k-1)*86400;
    [samples_h, samples_array, samples_flag, ...
        samples_susL, samples_susP, samples_tilt] = readframes(gps,gps+86400);
    save(['Samples_' num2str(gps,12) '.mat'],...
        'samples_h', 'samples_array', 'samples_flag', 'samples_susL', 'samples_susP', 'samples_tilt');
end