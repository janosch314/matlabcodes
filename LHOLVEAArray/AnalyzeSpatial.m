% Spatial spectra are read and analyzed to generate histograms.
%
% Jan Harms, August 19, 2018
%

clear all

set(0,'DefaultAxesFontSize',14);
set(0,'DefaultTextFontSize',14);

xy = coords_map();
c0 = 100; % minimal speed analyzed in spatial spectra

%% load samples
listing = dir('./data_spatial/Spatial*');

for s = 1:length(listing)
    gps = listing(s).name(16:end-4);
    
    disp(['Reading data from SpatialSpectra_' gps '.mat'])
    load(['./data_spatial/SpatialSpectra_' gps '.mat'])
    
    histprop(s).speed = zeros(length(map(1).kmax(:,1)),length(map));
    histprop(s).phi = zeros(length(map(1).kmax(:,1)),length(map));

    % --- collect data for hisograms --------------------------------------
    for fi = 1:length(map)
        ff(fi) = c0*map(fi).k0/(2*pi);
        histprop(s).speed(:,fi) = 2*pi*ff(fi)./sqrt(sum(map(fi).kmax.^2,2));
        histprop(s).phi(:,fi) = atan2(map(fi).kmax(:,2),map(fi).kmax(:,1));
    end
    
    % --- plot maps -------------------------------------------------------
%     cc =  [100,250,500,1000];
% 
%     for fi = 1:length(ff)
%         kx = linspace(-map(fi).k0,map(fi).k0,201);
%         ky = kx;
% 
%         [mv,mi] = max(map(fi).pp10(:));
%         [mx,my] = ind2sub(length(kx)*[1,1],mi);
% 
%         figure(1)
%         set(gcf, 'PaperSize',[10 8])
%         set(gcf, 'PaperPosition', [0 0 10 8])
%         clf
%         imagesc(kx,ky,map(fi).pp10/mv)
%         set(gca,'ydir','normal')
%         % CT = brewermap(128,'RdYlBu');
%         % colormap(flipud(CT));
%         hold on
%         for k = 1:length(cc)
%             plot(2*pi*ff(fi)/cc(k)*cos(linspace(0,2*pi,200)),...
%                 2*pi*ff(fi)/cc(k)*sin(linspace(0,2*pi,200)),'w','LineWidth',2)
%         end
%         for k = 1:length(cc)
%             text(2*pi*ff(fi)/cc(k)*cos(pi-0.6),...
%                 2*pi*ff(fi)/cc(k)*sin(pi-0.6),[num2str(cc(k)) 'm/s'],'Color','k','FontSize',16)
%         end
%         set(gca,'layer','top')
%         plot(ky(my),kx(mx),'ow','LineWidth',3)
%         hold off
%         shading flat
%         grid
%         caxis([0 1])
%         set(gca,'DataAspectRatio',[1 1 1])
%         xlabel('Wavenumber k_x [rad/m]')
%         ylabel('Wavenumber k_y [rad/m]')
%         cb = colorbar;
%         set(get(cb,'ylabel'),'string',['Spatial Spectrum at ' num2str(ff(fi),3) 'Hz'])
%         saveas(gcf,['./histoplots/SpatialSpectrum_' strrep(num2str(ff(fi)),'.','p') 'Hz_' gps '.png'])
%     end    

    clear map
end

%% plot propagation histograms

bb_speed = linspace(200,700,40);
bb_phi = linspace(-pi,pi,40);

histspeed = zeros(length(bb_speed),length(ff));
histphi = zeros(length(bb_phi),length(ff));
for s = 1:length(histprop)
    histspeed = histspeed+histc(histprop(s).speed,bb_speed,1);
    histphi = histphi+histc(histprop(s).phi,bb_phi,1);
end

figure(2)
set(gcf, 'PaperSize',[10 8])
set(gcf, 'PaperPosition', [0 0 10 8])
set(gcf,'DefaultAxesLineStyleOrder','-|--|:|-.')
clf
plot(bb_speed,histspeed')
grid
xlabel('Seismic speed [m/s]')
ylabel('Distribution')
legend(strcat(num2str(ff'),repmat('Hz',11,1)))
saveas(gcf,'./plots_histo/Speeds.png')

figure(3)
set(gcf, 'PaperSize',[10 8])
set(gcf, 'PaperPosition', [0 0 10 8])
set(gcf,'DefaultAxesLineStyleOrder','-|--|:|-.')
clf
plot(bb_phi*180/pi,histphi')
set(gca,'xlim',[-180 180])
grid
xlabel('Propagation azimuth [deg]')
ylabel('Distribution')
legend(strcat(num2str(ff'),repmat('Hz',11,1)))
saveas(gcf,'./plots_histo/Azimuths.png')