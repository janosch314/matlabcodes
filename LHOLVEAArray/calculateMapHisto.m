function map = calculateMapHisto(samples,xy,fs,T,nav)

[N,SN] = size(samples);

ff = linspace(0,fs/2,T*fs/2+1);

nfft = T*fs;
antialiasing = hann(nfft);

n_seg = nfft*nav;
ns = floor(N/n_seg); %number of spectra

ii = find(ff>5 & ff<25);

disp('Calculating coherences ...')
Caa = zeros(length(ii),SN,SN,ns);
for k = 1:ns
    for ni = 1:SN
%         newcpsd = cpsd(samples(1+(k-1)*n_seg:k*n_seg,ni:end),...
%             samples(1+(k-1)*n_seg:k*n_seg,ni),antialiasing,0,ff,fs);
        newcpsd = cpsd(samples(1+(k-1)*n_seg:k*n_seg,ni:end),...
            samples(1+(k-1)*n_seg:k*n_seg,ni),antialiasing,0,nfft);
        Caa(:,ni:end,ni,k) = newcpsd(ii,:);
        Caa(:,ni,ni:end,k) = conj(newcpsd(ii,:));
    end
end

dx = zeros(SN,SN); 
dy = zeros(SN,SN); 
for c1 = 1:SN
    for c2 = 1:SN
        dx(c1,c2) = xy(c2,1)-xy(c1,1);        
        dy(c1,c2) = xy(c2,2)-xy(c1,2);        
    end
end
        
disp('Calculating spatial spectra ...')
c = 200; %minimal seismic speed mapped
for fi = 1:length(ii)
    disp(['... at frequency ' num2str(ff(ii(fi)),3) 'Hz'])
    map(fi).kmax = zeros(ns,2);    
    map(fi).k0 = 2*pi*ff(ii(fi))/c;

    nk = 201;
    kx = linspace(-map(fi).k0,map(fi).k0,nk);
    ky = linspace(-map(fi).k0,map(fi).k0,nk);
    [kX, kY] = meshgrid(kx,ky);
    
    spectra = zeros(nk*nk,ns);
    for k = 1:ns
        caa = squeeze(Caa(fi,:,:,k));
        
        % complex-valued only because of numerical precision limits 
        % (so take real part)
        spectra(:,k) = real(sum((caa(:)*ones(1,nk*nk)).*exp(-1i*(dx(:)*(kX(:).')+dy(:)*kY(:).')),1));
        
        %extract maximum
        [mv,mi] = max(abs(spectra(:,k)));
        [mx,my] = ind2sub(nk*[1,1],mi);

        map(fi).kmax(k,:) = [kx(mx),ky(my)];
    end   
    
    spectra = spectra/max(spectra(:));
%     min(spectra(:))
%     max(spectra(:))

    nb = 500;
    bb = logspace(-5,0,nb);
    av = sum((spectra>=bb(1)).*(spectra<=bb(end)),2);

    hist2D = 100*histc(spectra,bb,2)./av(:,ones(1,nb));
%     map(fi).hist2D = reshape(hist2D,nk,nk,nb);

    acc = cumsum(hist2D,2);
    [~, i10] = min(abs(acc-10),[],2);
    [~, i50] = min(abs(acc-50),[],2);
    [~, i90] = min(abs(acc-90),[],2);
    map(fi).pp10 = reshape(bb(i10),nk,nk);
    map(fi).pp50 = reshape(bb(i50),nk,nk);
    map(fi).pp90 = reshape(bb(i90),nk,nk);
end


