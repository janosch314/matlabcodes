function samples_M0 = addTopMass6D(gps0,gps1)

addpath(genpath('/usr/share/ligotools/matlab/'));

system(char(['gw_data_find -o H -t H1_R -u file' ...
    ' -s ' num2str(gps0) ' -e ' num2str(gps1) ' -u file --lal-cache > frames_array.log']));

fid = fopen('frames_array.log'); 
columns = textscan(fid,'%s %s %s %s %s');
files_array = char(columns{5});
files_array = files_array(:,17:end);

N = length(files_array(:,1));
fs = 64;    %sampling frequency after decimating
T = 64;     %length of frame file in seconds

channels = {'H1:SUS-ITMY_M0_DAMP_L_IN1_DQ','H1:SUS-ITMY_M0_DAMP_P_IN1_DQ',...
    'H1:SUS-ITMY_M0_DAMP_R_IN1_DQ','H1:SUS-ITMY_M0_DAMP_T_IN1_DQ',...
    'H1:SUS-ITMY_M0_DAMP_V_IN1_DQ','H1:SUS-ITMY_M0_DAMP_Y_IN1_DQ'};

%% top mass OSEM channels
samples_M0 = zeros((gps1-gps0)*fs,6);

%read first frame
dT = str2double(files_array(1,end-16:end-7))+T-gps0;
dN = dT*fs;
disp(['Reading top mass OSEM data from ' files_array(1,:)])
try
    for k = 1:length(channels)
        ss = frgetvect(files_array(1,:),char(channels{k}),gps0,dT);
        ss = decimate(ss,4); %originally, 256Hz
        samples_M0(1:dN,k) = ss;
    end
catch exception
    disp(exception.identifier)
end
%read mid frames
for fi = 2:N-1
    gps = str2double(files_array(fi,end-16:end-7));  
    disp(['Reading top mass OSEM data from ' files_array(fi,:)])
    try
        for k = 1:length(channels)
            ss = frgetvect(files_array(fi,:),char(channels{k}),gps,T);
            ss = decimate(ss,4); %originally, 256Hz
            samples_M0(dN+1:dN+fs*T,k) = ss;
        end
    catch exception
        disp(exception.identifier)
    end
    dN = dN+T*fs;
end
%read last frame
gps = files_array(N,end-16:end-7);
dT = gps1-str2double(gps);
disp(['Reading top mass OSEM data from ' files_array(N,:)])
try
    for k = 1:length(channels)
        ss = frgetvect(files_array(N,:),char(channels{k}),gps,dT);
        ss = decimate(ss,4); %originally, 256Hz
        samples_M0(dN+1:dN+fs*dT,k) = ss;
    end
catch exception
    disp(exception.identifier)
end

