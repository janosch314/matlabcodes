% This code reads suspension point, and top-mass data from O2
% starting December 1, 2016, down-sampled to 64Hz.
%
% Jan Harms, August 7, 2018
%

clear all

set(0,'DefaultAxesFontSize',14);
set(0,'DefaultTextFontSize',14);

ns = 30; %number of array sensors
fs = 64;
nfft = 8*fs;
T = 86400;
N = T*fs;

antialiasing = hann(nfft);

%% load samples
listing = dir('Samples*');

for s = 46:length(listing)
    gps = listing(s).name(9:end-4);
    load(listing(s).name,'samples_susL','samples_susP','samples_h','samples_M0')
    
    if N==length(samples_h)
        disp(['Processing GPS ' num2str(gps,12) '...'])
        
        % pre-processing ------------------------------------------------------
        [b, a] = butter(4,2*3/fs,'high');
        samples_h = filtfilt(b, a, samples_h);
        for k = 1:2
            samples_susL(:,k) = filtfilt(b, a, samples_susL(:,k))/1e9;
            samples_susP(:,k) = filtfilt(b, a, samples_susP(:,k))/1e9;
        end

        % h(t) spectrogram to veto stretches ----------------------------------
        tt_select = linspace(0,T,N);
        ii_stretch = find((tt_select>=0*3600).*(tt_select<=24*3600));

        [~, ff, tt, SS] = spectrogram(samples_h(ii_stretch),antialiasing,0,nfft,fs);
        [~, fi1] = min(abs(ff-6));
        [~, fi2] = min(abs(ff-9.3));
        [~, fi3] = min(abs(ff-20));
        ii = find((mean(SS(fi1-3:fi1+3,:))>4e-42).*(SS(fi2,:)<1e-34).*(mean(SS(fi3-5:fi3+5,:))<1e-40));
        ii_select = (ii'*ones(1,nfft)-1)*nfft+1+ones(length(ii),1)*(0:nfft-1);
        ii_select = union(ii_select(:),[]);    

        mean_M0 = zeros(floor(N/nfft),6);
        for k = 1:(N/nfft)
            mean_M0(k,:) = mean(samples_M0((k-1)*nfft+1:k*nfft,:),1);
        end
        tt_M0 = linspace(0,floor(N/nfft)*nfft/fs,floor(N/nfft));

        figure(1)
        set(gcf, 'PaperSize',[8 6])
        set(gcf, 'PaperPosition', [0 0 8 6])
        clf
        pcolor(tt(1:3:end)/3600,ff,log10(SS(:,1:3:end))/2)
        shading flat
        set(gca,'yscale','log','ylim',[5 25])
        caxis([-22 -17])
        if ~isempty(ii)
            hold on
            plot(tt(ii)/3600,10,'r.')
            hold off
        end
        cb = colorbar;
        xlabel('Time [h]')
        ylabel('Frequency [Hz]')
        set(get(cb,'ylabel'),'string','Strain spectrum, log10 [1/\surdHz]')
        saveas(gcf,['./plots/Spectrogram_h_' gps '.fig'])
            
        if ~isempty(ii)
            CsLYsLY = cpsd(samples_susL(ii_stretch(ii_select),2),samples_susL(ii_stretch(ii_select),2),antialiasing,0,nfft);
            CsPYsPY = cpsd(samples_susP(ii_stretch(ii_select),2),samples_susP(ii_stretch(ii_select),2),antialiasing,0,nfft);
            Chh = cpsd(samples_h(ii_stretch(ii_select)),samples_h(ii_stretch(ii_select)),antialiasing,0,nfft);
            
            % correlation between ITM suspension points (L,P) and DARM ----------------------------
            CsLYh = cpsd(samples_susL(ii_stretch(ii_select),2),samples_h(ii_stretch(ii_select)),antialiasing,0,nfft);
            cLYh = CsLYh./sqrt(CsLYsLY.*Chh);
            trLYh = CsLYh./CsLYsLY;
            
            CsPYh = cpsd(samples_susP(ii_stretch(ii_select),2),samples_h(ii_stretch(ii_select)),antialiasing,0,nfft);
            cPYh = CsPYh./sqrt(CsPYsPY.*Chh);
            trPYh = CsPYh./CsPYsPY;
                   
            Nseg = length(ii); % this number is required later to combine different days            

            save(['./data/ChargeCoupling_' gps '.mat'],'Chh','CsLYsLY',...
                'CsPYsPY','CsLYh','CsPYh','mean_M0','tt_M0','ff','Nseg')
            
            clear samples_susL samples_susP samples_h samples_M0
        else
            disp(['GPS: ' num2str(gps,12)])
            disp(['T*fs = ' num2str(N), ', length(samples_h) = ' num2str(length(samples_h)) ...
                ', length(samples_tilt) = ' num2str(length(samples_tilt))])
        end
    end
end

