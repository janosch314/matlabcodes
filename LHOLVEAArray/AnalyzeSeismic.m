% This code reads array, tiltmeter, suspension point, DARM data from O2
% starting December 1, 2016, down-sampled to 64Hz. These files were
% produced with DownSampler.m and readframes.m. 
% 
% The data are processed to show time series, spectra, coherence, transfer
% functions, etc. One of the main processing steps is the selection of
% high-quality data segments. This is done with the spectrogram output
% using spectral selection criteria.
%
% Jan Harms, March 31, 2018
%

clear all

set(0,'DefaultAxesFontSize',14);
set(0,'DefaultTextFontSize',14);

ns = 30; %number of array sensors
fs = 64;
T = 8;
nfft = T*fs;
N = 86400*fs;

antialiasing = hann(nfft);

%% load samples
listing = dir('Samples*');

for s = 1:length(listing)
    gps = listing(s).name(9:end-4);
    load(listing(s).name,'samples_array','samples_tilt','samples_h')
    
    if N==length(samples_h) && N==length(samples_tilt)
        disp(['Processing GPS ' num2str(gps,12) '...'])
        
        % pre-processing ------------------------------------------------------
        [b, a] = butter(4,2*3/fs,'high');
        samples_h = filtfilt(b, a, samples_h);
        for k = 1:ns
            samples_array(:,k) = filtfilt(b,a,samples_array(:,k))/1e9;
        end
        samples_tilt = filtfilt(b,a,samples_tilt);

        % h(t) spectrogram to veto stretches ----------------------------------
        [~, ff, tt, SS] = spectrogram(samples_h,antialiasing,0,nfft,fs);
        [~, fi1] = min(abs(ff-6));
        [~, fi2] = min(abs(ff-9.3));
        [~, fi3] = min(abs(ff-20));
        ii = find((mean(SS(fi1-3:fi1+3,:))>4e-42).*(SS(fi2,:)<1e-34).*(mean(SS(fi3-5:fi3+5,:))<1e-40));
        ii_select = (ii'*ones(1,nfft)-1)*nfft+1+ones(length(ii),1)*(0:nfft-1);
        ii_select = union(ii_select(:),[]);
        
        
        if ~isempty(ii)
            [SS,ff,tt,PP] = spectrogram(samples_tilt(ii_select),antialiasing,0,nfft,fs);
            
            nb = 200;
            
            bb_tilt = logspace(-12,-7,nb); 

            av_nums = sum((sqrt(PP)>bb_tilt(1)).*(sqrt(PP)<bb_tilt(end)),2);
            histo_tilt = 100*histc(sqrt(PP),bb_tilt,2)./av_nums(:,ones(1,nb));

            specdist = cumsum(histo_tilt,2);
            [~, p10] = min(abs(specdist-10),[],2);
            [~, p50] = min(abs(specdist-50),[],2);
            [~, p90] = min(abs(specdist-90),[],2);

            pp10_tilt = bb_tilt(p10);
            pp50_tilt = bb_tilt(p50);
            pp90_tilt = bb_tilt(p90);
            
            bb_seis = logspace(-10,-5,nb); %range of seismic spectral densities         
            for k = 1:length(samples_array(1,:))   
                [SS,ff,tt,PP] = spectrogram(samples_array(ii_select,k),antialiasing,0,nfft,fs);
                
                av_nums = sum((sqrt(PP)>bb_seis(1)).*(sqrt(PP)<bb_seis(end)),2);
                array(k).histo_seis = 100*histc(sqrt(PP),bb_seis,2)./av_nums(:,ones(1,nb));

                specdist = cumsum(array(k).histo_seis,2);
                [~, p10] = min(abs(specdist-10),[],2);
                [~, p50] = min(abs(specdist-50),[],2);
                [~, p90] = min(abs(specdist-90),[],2);

                array(k).pp10_seis = bb_seis(p10);
                array(k).pp50_seis = bb_seis(p50);
                array(k).pp90_seis = bb_seis(p90);
            end

            save(['./data_seismic/ArraySpectra_' gps '.mat'],...
                'histo_tilt','pp10_tilt','pp50_tilt','pp90_tilt','bb_tilt',...
                'bb_seis','array','ff')
        end
    end
end


