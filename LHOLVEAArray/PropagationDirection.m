% This code reads array and h(t) data from O2 starting December 1, 2016, 
% down-sampled to 64Hz. 
% 
% Strain data are used to select data stretches, and array data are
% analyzed to produce spatial spectra.
%
% Jan Harms, August 19, 2018
%

clear all

set(0,'DefaultAxesFontSize',14);
set(0,'DefaultTextFontSize',14);

ns = 30; %number of array sensors
fs = 64;
T = 8;
nfft = T*fs;

N = 86400*fs;

antialiasing = hann(nfft);

nav = 100; % number of averages of FFTs to calculate correlations for maps
xy = coords_map();

%% load samples
listing = dir('Samples*');

for s = 1:length(listing)
    gps = listing(s).name(9:end-4);
    
    % samples_h is required for data selection
    load(listing(s).name,'samples_array','samples_h')
    
    if N==length(samples_h)
        disp(['Processing GPS ' num2str(gps,12) '...'])
        
        % pre-processing ------------------------------------------------------
        [b, a] = butter(4,2*3/fs,'high');
        samples_h = filtfilt(b, a, samples_h);
        for k = 1:ns
            samples_array(:,k) = filtfilt(b,a,samples_array(:,k))/1e9;
        end

        % h(t) spectrogram to veto stretches ----------------------------------
        [~, ff, tt, SS] = spectrogram(samples_h,antialiasing,0,nfft,fs);
        [~, fi1] = min(abs(ff-6));
        [~, fi2] = min(abs(ff-9.3));
        [~, fi3] = min(abs(ff-20));
        ii = find((mean(SS(fi1-3:fi1+3,:))>4e-42).*(SS(fi2,:)<1e-34).*(mean(SS(fi3-5:fi3+5,:))<1e-40));
        ii_select = (ii'*ones(1,nfft)-1)*nfft+1+ones(length(ii),1)*(0:nfft-1);
        ii_select = union(ii_select(:),[]);

        if ~isempty(ii)
            % calculate spatial spectrum for selected frequencies ff
            map = calculateMapHisto(samples_array(ii_select,:),xy,fs,T,nav);    
            save(['./data_spatial/SpatialSpectra_' gps '.mat'],'map','-v7.3')
            
            clear samples_array samples_h
        else
            disp(['GPS: ' num2str(gps,12)])
            disp(['T*fs = ' num2str(N), ', length(samples_h) = ' num2str(length(samples_h))])
        end
    end
end

