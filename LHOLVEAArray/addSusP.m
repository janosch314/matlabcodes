function samples_susP = addSusP(gps0,gps1)

addpath(genpath('/usr/share/ligotools/matlab/'));

%% array data, tiltmeter
system(char(['gw_data_find -o H -t H1_R -u file' ...
    ' -s ' num2str(gps0) ' -e ' num2str(gps1) ' -u file --lal-cache > frames_array.log']));

fid = fopen('frames_array.log'); 
columns = textscan(fid,'%s %s %s %s %s');
files_array = char(columns{5});
files_array = files_array(:,17:end);

N = length(files_array(:,1));
fs = 64;    %sampling frequency after decimating
T = 64;     %length of frame file in seconds

%% suspension points
samples_susP = zeros((gps1-gps0)*fs,2);

%read first frame
dT = str2double(files_array(1,end-16:end-7))+T-gps0;
dN = dT*fs;
disp(['Reading suspension data from ' files_array(1,:)])
try
    ss = frgetvect(files_array(1,:),char('H1:ISI-ITMX_SUSPOINT_ITMX_EUL_P_DQ'),gps0,dT);
    ss = decimate(ss,16); %originally, 1024Hz
    samples_susP(1:dN,1) = ss;
    ss = frgetvect(files_array(1,:),char('H1:ISI-ITMY_SUSPOINT_ITMY_EUL_P_DQ'),gps0,dT);
    ss = decimate(ss,16); %originally, 1024Hz
    samples_susP(1:dN,2) = ss;
catch exception
    disp(exception.identifier)
end
%read mid frames
for fi = 2:N-1
    gps = str2double(files_array(fi,end-16:end-7));  
    disp(['Reading suspension data from ' files_array(fi,:)])
    try
        ss = frgetvect(files_array(fi,:),char('H1:ISI-ITMX_SUSPOINT_ITMX_EUL_P_DQ'),gps,T);
        ss = decimate(ss,16);
        samples_susP(dN+1:dN+fs*T,1) = ss;
        ss = frgetvect(files_array(fi,:),char('H1:ISI-ITMY_SUSPOINT_ITMY_EUL_P_DQ'),gps,T);
        ss = decimate(ss,16);
        samples_susP(dN+1:dN+fs*T,2) = ss;
    catch exception
        disp(exception.identifier)
    end
    dN = dN+T*fs;
end
%read last frame
gps = files_array(N,end-16:end-7);
dT = gps1-str2double(gps);
disp(['Reading suspension data from ' files_array(N,:)])
try
    ss = frgetvect(files_array(N,:),char('H1:ISI-ITMX_SUSPOINT_ITMX_EUL_P_DQ'),gps,dT);
    ss = decimate(ss,16);
    samples_susP(dN+1:dN+fs*dT,1) = ss;
    ss = frgetvect(files_array(N,:),char('H1:ISI-ITMY_SUSPOINT_ITMY_EUL_P_DQ'),gps,dT);
    ss = decimate(ss,16);
    samples_susP(dN+1:dN+fs*dT,2) = ss;
catch exception
    disp(exception.identifier)
end

