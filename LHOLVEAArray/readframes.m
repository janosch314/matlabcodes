function [samples_h, samples_array, samples_flag, ...
    samples_susL, samples_susP, samples_tilt] = readframes(gps0,gps1)

% gps0 = tconvert('2016/12/01 00:00:00');
% gps1 = gps0+3600;

addpath(genpath('/usr/share/ligotools/matlab/'));

%% DARM
system(char(['gw_data_find -o H -t H1_HOFT_C00 -u file' ...
    ' -s ' num2str(gps0) ' -e ' num2str(gps1) ' -u file --lal-cache > frames_h.log']));

% disp(['gps0=' num2str(gps0,12) ', gps1=' num2str(gps1,12)])

fid = fopen('frames_h.log'); 
columns = textscan(fid,'%s %s %s %s %s');
files_h = char(columns{5});
gps_h = char(columns{3});
dT_h = char(columns{4});
files_h = files_h(:,17:end);

N = length(files_h(:,1));
fs = 64;    %sampling frequency after decimating
fs_flag = 16;

% for k = 1:N
%     disp(files_h(k,:))
% end

samples_h = zeros((gps1-gps0)*fs,1); 
samples_flag = zeros((gps1-gps0)*fs_flag,1);

%read first frame
dT = str2double(gps_h(1,:))-gps0+str2double(dT_h(1,:));
dN = fs*dT;
disp(['Reading h(t) data from ' files_h(1,:)])
ss = frgetvect(files_h(1,:),char('H1:GDS-CALIB_STRAIN'),gps0,dT);
ss = decimate(decimate(ss,16),16); %originally, 16384Hz
samples_h(1:dN) = ss;

dN_flag = fs_flag*dT;
ss = frgetvect(files_h(1,:),char('H1:DMT-DQ_VECTOR'),gps0,dT);
samples_flag(1:dN_flag) = ss;

%read mid frames
for fi = 2:N-1
    gps = str2double(gps_h(fi,:));
    dT = str2double(dT_h(fi,:));
    
    disp(['Reading h(t) data from ' files_h(fi,:)])
    ss = frgetvect(files_h(fi,:),char('H1:GDS-CALIB_STRAIN'),gps,dT);
    ss = decimate(decimate(ss,16),16);
    samples_h(dN+1:dN+fs*dT) = ss;
    dN = dN+dT*fs;
    
    ss = frgetvect(files_h(fi,:),char('H1:DMT-DQ_VECTOR'),gps,dT);
    samples_flag(dN_flag+1:dN_flag+fs_flag*dT) = ss;
    dN_flag = dN_flag+dT*fs_flag;
end

%read last frame
gps = str2double(gps_h(N,:));
dT = gps1-gps;
disp(['Reading h(t) data from ' files_h(N,:)])
ss = frgetvect(files_h(N,:),char('H1:GDS-CALIB_STRAIN'),gps,dT);
ss = decimate(decimate(ss,16),16);
samples_h(dN+1:dN+dT*fs) = ss;

ss = frgetvect(files_h(N,:),char('H1:DMT-DQ_VECTOR'),gps,dT);
samples_flag(dN_flag+1:dN_flag+dT*fs_flag) = ss;

%% array data, tiltmeter
system(char(['gw_data_find -o H -t H1_R -u file' ...
    ' -s ' num2str(gps0) ' -e ' num2str(gps1) ' -u file --lal-cache > frames_array.log']));

fid = fopen('frames_array.log'); 
columns = textscan(fid,'%s %s %s %s %s');
files_array = char(columns{5});
files_array = files_array(:,17:end);

N = length(files_array(:,1));
T = 64;     %length of frame file in seconds

samples_array = zeros((gps1-gps0)*fs,30);
samples_tilt = zeros((gps1-gps0)*fs,1);

%read first frame
dT = str2double(files_array(1,end-16:end-7))+T-gps0;
dN = dT*fs;
disp(['Reading seismic data from ' files_array(1,:)])
for k = 1:30
    try
        ss = frgetvect(files_array(1,:),char(['H1:NGN-CS_L4C_Z_' num2str(k) '_OUT_DQ']),gps0,dT);
        ss = decimate(ss,8); %originally, 512Hz
        samples_array(1:dN,k) = ss;
    catch exception
        disp(exception.identifier)
    end
end
try
    ss1 = frgetvect(files_array(1,:),char('H1:NGN-CS_CBRS_RY_PZT1_CTRL_IN1_DQ'),gps0,dT);
    ss2 = frgetvect(files_array(1,:),char('H1:NGN-CS_CBRS_RY_PZT2_CTRL_IN1_DQ'),gps0,dT);
    ss3 = frgetvect(files_array(1,:),char('H1:NGN-CS_CBRS_RY_PZT1_MASTER_OUT_DQ'),gps0,dT);
    ss4 = frgetvect(files_array(1,:),char('H1:NGN-CS_CBRS_RY_PZT2_MASTER_OUT_DQ'),gps0,dT);
    ss1 = ss1*1.6e-10; ss2 = ss2*1.23e-10; ss3 = ss3*2.8e-10; ss4 = ss4*2.8e-10;
    ss3 = round(ss3/2.8e-10)*2.8e-10; ss4 = round(ss4/2.8e-10)*2.8e-10;
    PD1S = ss1 + ss3;   %This is corrected tilt signal in IFO 1
    PD2S = ss2 + ss4;   %This is corrected tilt signal in IFO 2
    ss = (PD1S-PD2S)/2;
    ss = decimate(ss,8); %originally, 512Hz
    samples_tilt(1:dN) = ss;
catch exception
    disp(exception.identifier)
end
%read mid frames
for fi = 2:N-1
    gps = str2double(files_array(fi,end-16:end-7));  
    disp(['Reading seismic data from ' files_array(fi,:)])
    for k = 1:30
        try
            ss = frgetvect(files_array(fi,:),char(['H1:NGN-CS_L4C_Z_' num2str(k) '_OUT_DQ']),gps,T);
            ss = decimate(ss,8);
            samples_array(dN+1:dN+fs*T,k) = ss;
        catch exception
            disp(exception.identifier)
        end
    end
    try
        ss1 = frgetvect(files_array(fi,:),char('H1:NGN-CS_CBRS_RY_PZT1_CTRL_IN1_DQ'),gps,T);
        ss2 = frgetvect(files_array(fi,:),char('H1:NGN-CS_CBRS_RY_PZT2_CTRL_IN1_DQ'),gps,T);
        ss3 = frgetvect(files_array(fi,:),char('H1:NGN-CS_CBRS_RY_PZT1_MASTER_OUT_DQ'),gps,T);
        ss4 = frgetvect(files_array(fi,:),char('H1:NGN-CS_CBRS_RY_PZT2_MASTER_OUT_DQ'),gps,T);
        ss1 = ss1*1.6e-10; ss2 = ss2*1.23e-10; ss3 = ss3*2.8e-10; ss4 = ss4*2.8e-10;
        ss3 = round(ss3/2.8e-10)*2.8e-10; ss4 = round(ss4/2.8e-10)*2.8e-10;
        PD1S = ss1 + ss3;   %This is corrected tilt signal in IFO 1
        PD2S = ss2 + ss4;   %This is corrected tilt signal in IFO 2
        ss = (PD1S-PD2S)/2;
        ss = decimate(ss,8); %originally, 512Hz
        samples_tilt(dN+1:dN+fs*T) = ss;
    catch exception
        disp(exception.identifier)
    end
    dN = dN+T*fs;
end
%read last frame
gps = files_array(N,end-16:end-7);
dT = gps1-str2double(gps);
disp(['Reading seismic data from ' files_array(N,:)])
for k = 1:30
    try
        ss = frgetvect(files_array(N,:),char(['H1:NGN-CS_L4C_Z_' num2str(k) '_OUT_DQ']),gps,dT);
        ss = decimate(ss,8);
        samples_array(dN+1:dN+fs*dT,k) = ss;
    catch exception
        disp(exception.identifier)
    end
end
try
    ss1 = frgetvect(files_array(N,:),char('H1:NGN-CS_CBRS_RY_PZT1_CTRL_IN1_DQ'),gps,dT);
    ss2 = frgetvect(files_array(N,:),char('H1:NGN-CS_CBRS_RY_PZT2_CTRL_IN1_DQ'),gps,dT);
    ss3 = frgetvect(files_array(N,:),char('H1:NGN-CS_CBRS_RY_PZT1_MASTER_OUT_DQ'),gps,dT);
    ss4 = frgetvect(files_array(N,:),char('H1:NGN-CS_CBRS_RY_PZT2_MASTER_OUT_DQ'),gps,dT);
    ss1 = ss1*1.6e-10; ss2 = ss2*1.23e-10; ss3 = ss3*2.8e-10; ss4 = ss4*2.8e-10;
    ss3 = round(ss3/2.8e-10)*2.8e-10; ss4 = round(ss4/2.8e-10)*2.8e-10;
    PD1S = ss1 + ss3;   %This is corrected tilt signal in IFO 1
    PD2S = ss2 + ss4;   %This is corrected tilt signal in IFO 2
    ss = (PD1S-PD2S)/2;
    ss = decimate(ss,8); %originally, 512Hz
    samples_tilt(dN+1:dN+fs*dT) = ss;
catch exception
    disp(exception.identifier)
end
%% suspension points
samples_susL = zeros((gps1-gps0)*fs,2);
samples_susP = zeros((gps1-gps0)*fs,2);

%read first frame
dT = str2double(files_array(1,end-16:end-7))+T-gps0;
dN = dT*fs;
disp(['Reading suspension data from ' files_array(1,:)])
try
    ss = frgetvect(files_array(1,:),char('H1:ISI-ITMX_SUSPOINT_ITMX_EUL_L_DQ'),gps0,dT);
    ss = decimate(ss,16); %originally, 1024Hz
    samples_susL(1:dN,1) = ss;
    ss = frgetvect(files_array(1,:),char('H1:ISI-ITMY_SUSPOINT_ITMY_EUL_L_DQ'),gps0,dT);
    ss = decimate(ss,16); %originally, 1024Hz
    samples_susL(1:dN,2) = ss;
    ss = frgetvect(files_array(1,:),char('H1:ISI-ITMX_SUSPOINT_ITMX_EUL_P_DQ'),gps0,dT);
    ss = decimate(ss,16); %originally, 1024Hz
    samples_susP(1:dN,1) = ss;
    ss = frgetvect(files_array(1,:),char('H1:ISI-ITMY_SUSPOINT_ITMY_EUL_P_DQ'),gps0,dT);
    ss = decimate(ss,16); %originally, 1024Hz
    samples_susP(1:dN,2) = ss;
catch exception
    disp(exception.identifier)
end
%read mid frames
for fi = 2:N-1
    gps = str2double(files_array(fi,end-16:end-7));  
    disp(['Reading suspension data from ' files_array(fi,:)])
    try
        ss = frgetvect(files_array(fi,:),char('H1:ISI-ITMX_SUSPOINT_ITMX_EUL_L_DQ'),gps,T);
        ss = decimate(ss,16);
        samples_susL(dN+1:dN+fs*T,1) = ss;
        ss = frgetvect(files_array(fi,:),char('H1:ISI-ITMY_SUSPOINT_ITMY_EUL_L_DQ'),gps,T);
        ss = decimate(ss,16);
        samples_susL(dN+1:dN+fs*T,2) = ss;
        ss = frgetvect(files_array(fi,:),char('H1:ISI-ITMX_SUSPOINT_ITMX_EUL_P_DQ'),gps,T);
        ss = decimate(ss,16);
        samples_susP(dN+1:dN+fs*T,1) = ss;
        ss = frgetvect(files_array(fi,:),char('H1:ISI-ITMY_SUSPOINT_ITMY_EUL_P_DQ'),gps,T);
        ss = decimate(ss,16);
        samples_susP(dN+1:dN+fs*T,2) = ss;
    catch exception
        disp(exception.identifier)
    end
    dN = dN+T*fs;
end
%read last frame
gps = files_array(N,end-16:end-7);
dT = gps1-str2double(gps);
disp(['Reading suspension data from ' files_array(N,:)])
try
    ss = frgetvect(files_array(N,:),char('H1:ISI-ITMX_SUSPOINT_ITMX_EUL_L_DQ'),gps,dT);
    ss = decimate(ss,16);
    samples_susL(dN+1:dN+fs*dT,1) = ss;
    ss = frgetvect(files_array(N,:),char('H1:ISI-ITMY_SUSPOINT_ITMY_EUL_L_DQ'),gps,dT);
    ss = decimate(ss,16);
    samples_susL(dN+1:dN+fs*dT,2) = ss;
    ss = frgetvect(files_array(N,:),char('H1:ISI-ITMX_SUSPOINT_ITMX_EUL_P_DQ'),gps,dT);
    ss = decimate(ss,16);
    samples_susP(dN+1:dN+fs*dT,1) = ss;
    ss = frgetvect(files_array(N,:),char('H1:ISI-ITMY_SUSPOINT_ITMY_EUL_P_DQ'),gps,dT);
    ss = decimate(ss,16);
    samples_susP(dN+1:dN+fs*dT,2) = ss;
catch exception
    disp(exception.identifier)
end

