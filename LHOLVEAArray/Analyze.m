% This code reads array, tiltmeter, suspension point, DARM data from O2
% starting December 1, 2016, down-sampled to 64Hz. These files were
% produced with DownSampler.m and readframes.m. 
% 
% The data are processed to show time series, spectra, coherence, transfer
% functions, etc. One of the main processing steps is the selection of
% high-quality data segments. This is done with the spectrogram output
% using spectral selection criteria.
%
% Jan Harms, March 31, 2018
%

clear all

set(0,'DefaultAxesFontSize',14);
set(0,'DefaultTextFontSize',14);

ns = 30; %number of array sensors
fs = 64;
nfft = 8*fs;
T = 86400;
N = T*fs;

antialiasing = hann(nfft);

slide = 1;

%% load samples
listing = dir('Samples*');

for s = 55:length(listing)
    gps = listing(s).name(9:end-4);
    load(listing(s).name,'samples_susL','samples_susP','samples_tilt','samples_h')
    
    if N==length(samples_h) && N==length(samples_tilt)
        disp(['Processing GPS ' num2str(gps,12) '...'])
        
        % pre-processing ------------------------------------------------------
        [b, a] = butter(4,2*3/fs,'high');
        samples_h = filtfilt(b, a, samples_h);
%         for k = 1:ns
%             samples_array(:,k) = filtfilt(b,a,samples_array(:,k))/1e9;
%         end
        samples_tilt = filtfilt(b,a,samples_tilt);
        for k = 1:2
            samples_susL(:,k) = filtfilt(b, a, samples_susL(:,k))/1e9;
            samples_susP(:,k) = filtfilt(b, a, samples_susP(:,k))/1e9;
        end
        
        if slide % need to slide several time series to give error measurements for all transfer functions
            nslide = 141*nfft;
            keeper = samples_tilt(1:nslide);
            samples_tilt(1:nslide) = [];
            samples_tilt(end+1:end+nslide) = keeper;
            
            nslide = 71*nfft;
            keeper = samples_susL(1:nslide,:);
            samples_susL(1:nslide,:) = [];
            samples_susL(end+1:end+nslide,:) = keeper;
            keeper = samples_susP(1:nslide,:);
            samples_susP(1:nslide,:) = [];
            samples_susP(end+1:end+nslide,:) = keeper;
        end

        % h(t) spectrogram to veto stretches ----------------------------------
        tt_select = linspace(0,T,N);
        ii_stretch = find((tt_select>=0*3600).*(tt_select<=24*3600));

        [~, ff, tt, SS] = spectrogram(samples_h(ii_stretch),antialiasing,0,nfft,fs);
        [~, fi1] = min(abs(ff-6));
        [~, fi2] = min(abs(ff-9.3));
        [~, fi3] = min(abs(ff-20));
        ii = find((mean(SS(fi1-3:fi1+3,:))>4e-42).*(SS(fi2,:)<1e-34).*(mean(SS(fi3-5:fi3+5,:))<1e-40));
        ii_select = (ii'*ones(1,nfft)-1)*nfft+1+ones(length(ii),1)*(0:nfft-1);
        ii_select = union(ii_select(:),[]);
        
        if ~slide 
            figure(1)
            set(gcf, 'PaperSize',[8 6])
            set(gcf, 'PaperPosition', [0 0 8 6])
            clf
            pcolor(tt(1:3:end)/3600,ff,log10(SS(:,1:3:end))/2)
            shading flat
            set(gca,'yscale','log','ylim',[5 25])
            caxis([-22 -17])
            if ~isempty(ii)
                hold on
                plot(tt(ii)/3600,10,'r.')
                hold off
            end
            cb = colorbar;
            xlabel('Time [h]')
            ylabel('Frequency [Hz]')
            set(get(cb,'ylabel'),'string','Strain spectrum, log10 [1/\surdHz]')
            saveas(gcf,['./plots/Spectrogram_h_' gps '.fig'])      
        end

        if ~isempty(ii)
            % correlation between ground tilt and ITM suspension points (L,P) -------
            Ctt = cpsd(samples_tilt(ii_stretch(ii_select)),samples_tilt(ii_stretch(ii_select)),antialiasing,0,nfft);

            CsLXsLX = cpsd(samples_susL(ii_stretch(ii_select),1),samples_susL(ii_stretch(ii_select),1),antialiasing,0,nfft);
            CtsLX = cpsd(samples_tilt(ii_stretch(ii_select)),samples_susL(ii_stretch(ii_select),1),antialiasing,0,nfft);
            cLX = CtsLX./sqrt(Ctt.*CsLXsLX);
            trLX = CtsLX./Ctt;
            CsLYsLY = cpsd(samples_susL(ii_stretch(ii_select),2),samples_susL(ii_stretch(ii_select),2),antialiasing,0,nfft);
            CtsLY = cpsd(samples_tilt(ii_stretch(ii_select)),samples_susL(ii_stretch(ii_select),2),antialiasing,0,nfft);
            cLY = CtsLY./sqrt(Ctt.*CsLYsLY);
            trLY = CtsLY./Ctt;
            
            CsPXsPX = cpsd(samples_susP(ii_stretch(ii_select),1),samples_susP(ii_stretch(ii_select),1),antialiasing,0,nfft);
            CtsPX = cpsd(samples_tilt(ii_stretch(ii_select)),samples_susP(ii_stretch(ii_select),1),antialiasing,0,nfft);
            cPX = CtsPX./sqrt(Ctt.*CsPXsPX);
            trPX = CtsPX./Ctt;
            CsPYsPY = cpsd(samples_susP(ii_stretch(ii_select),2),samples_susP(ii_stretch(ii_select),2),antialiasing,0,nfft);
            CtsPY = cpsd(samples_tilt(ii_stretch(ii_select)),samples_susP(ii_stretch(ii_select),2),antialiasing,0,nfft);
            cPY = CtsPY./sqrt(Ctt.*CsPYsPY);
            trPY = CtsPY./Ctt;

             % correlation between ground tilt and DARM ----------------------------
            Chh = cpsd(samples_h(ii_stretch(ii_select)),samples_h(ii_stretch(ii_select)),antialiasing,0,nfft);
            Cth = cpsd(samples_tilt(ii_stretch(ii_select)),samples_h(ii_stretch(ii_select)),antialiasing,0,nfft);
            ch = Cth./sqrt(Ctt.*Chh);
            trh = Cth./Ctt;
            
            % correlation between ITM suspension points (L,P) and DARM ----------------------------
            CsLXh = cpsd(samples_susL(ii_stretch(ii_select),1),samples_h(ii_stretch(ii_select)),antialiasing,0,nfft);
            CsLYh = cpsd(samples_susL(ii_stretch(ii_select),2),samples_h(ii_stretch(ii_select)),antialiasing,0,nfft);
            cLXh = CsLXh./sqrt(CsLXsLX.*Chh);
            cLYh = CsLYh./sqrt(CsLYsLY.*Chh);
            trLXh = CsLXh./CsLXsLX;
            trLYh = CsLYh./CsLYsLY;
            
            CsPXh = cpsd(samples_susP(ii_stretch(ii_select),1),samples_h(ii_stretch(ii_select)),antialiasing,0,nfft);
            CsPYh = cpsd(samples_susP(ii_stretch(ii_select),2),samples_h(ii_stretch(ii_select)),antialiasing,0,nfft);
            cPXh = CsPXh./sqrt(CsPXsPX.*Chh);
            cPYh = CsPYh./sqrt(CsPYsPY.*Chh);
            trPXh = CsPXh./CsPXsPX;
            trPYh = CsPYh./CsPYsPY;
            
            Nseg = length(ii); % this number is required later to combine different days            
            
            if slide
                save(['./data_slide/Coherence_' gps '_slide.mat'],'Ctt','Chh','Cth',...
                    'CsLXsLX','CsLYsLY','CtsLX','CtsLY','CsLXh','CsLYh',...
                    'CsPXsPX','CsPYsPY','CtsPX','CtsPY','CsPXh','CsPYh','ff','Nseg')
            else
                save(['./data/Coherence_' gps '.mat'],'Ctt','Chh','Cth',...
                    'CsLXsLX','CsLYsLY','CtsLX','CtsLY','CsLXh','CsLYh',...
                    'CsPXsPX','CsPYsPY','CtsPX','CtsPY','CsPXh','CsPYh','ff','Nseg')
            end
            
            if ~slide
                fi = find((ff>5).*(ff<25));

                figure(2)
                set(gcf, 'PaperSize',[8 6])
                set(gcf, 'PaperPosition', [0 0 8 6])
                clf
                semilogx(ff,abs(cLX),ff,abs(cLY))
                grid
                axis([5 25 0 2*max(abs(cLX(fi)))])
                xlabel('Frequency [Hz]')
                ylabel('Coherence tilt<>sus L')
                legend('ITMX','ITMY')
                saveas(gcf,['./plots/Coherence_tilt2susL_' gps '.fig'])  

                figure(3)
                set(gcf, 'PaperSize',[8 6])
                set(gcf, 'PaperPosition', [0 0 8 6])
                clf
                semilogx(ff,abs(cPX),ff,abs(cPY))
                grid
                axis([5 25 0 2*max(abs(cPX(fi)))])
                xlabel('Frequency [Hz]')
                ylabel('Coherence tilt<>sus P')
                legend('ITMX','ITMY')
                saveas(gcf,['./plots/Coherence_tilt2susP_' gps '.fig']) 

                figure(4)
                set(gcf, 'PaperSize',[8 12])
                set(gcf, 'PaperPosition', [0 0 8 12])
                set(gcf, 'Position', [0 0 600 900])
                clf
                subplot(2,1,1)
                loglog(ff,abs(trLX),ff,abs(trLY))
                grid
                axis([5 25 1e-5 1])
                xlabel('Frequency [Hz]')
                ylabel('Magnitude tilt<>sus L [m/rad]')
                legend('ITMX','ITMY')
                subplot(2,1,2)
                semilogx(ff,angle(trLX),ff,angle(trLY))
                grid
                axis([5 25 -pi pi])
                xlabel('Frequency [Hz]')
                ylabel('Phase tilt<>sus L [rad]')
                legend('ITMX','ITMY')
                saveas(gcf,['./plots/Transfer_tilt2susL_' gps '.fig'])      

                figure(5)
                set(gcf, 'PaperSize',[8 12])
                set(gcf, 'PaperPosition', [0 0 8 12])
                set(gcf, 'Position', [0 0 600 900])
                clf
                subplot(2,1,1)
                loglog(ff,abs(trPX),ff,abs(trPY))
                grid
                axis([5 25 1e-5 1])
                xlabel('Frequency [Hz]')
                ylabel('Magnitude tilt<>sus P [rad/rad]')
                legend('ITMX','ITMY')
                subplot(2,1,2)
                semilogx(ff,angle(trPX),ff,angle(trPY))
                grid
                axis([5 25 -pi pi])
                xlabel('Frequency [Hz]')
                ylabel('Phase tilt<>sus P [rad]')
                legend('ITMX','ITMY')
                saveas(gcf,['./plots/Transfer_tilt2susP_' gps '.fig'])      

                figure(6)
                set(gcf, 'PaperSize',[8 6])
                set(gcf, 'PaperPosition', [0 0 8 6])
                clf
                semilogx(ff,abs(cLXh),ff,abs(cLYh))
                grid
                axis([5 25 0 2*max(abs(cLXh(fi)))])
                xlabel('Frequency [Hz]')
                ylabel('Coherence sus L<>h(t)')
                legend('ITMX','ITMY')
                saveas(gcf,['./plots/Coherence_susL2h_' gps '.fig'])   

                figure(7)
                set(gcf, 'PaperSize',[8 6])
                set(gcf, 'PaperPosition', [0 0 8 6])
                clf
                semilogx(ff,abs(cPXh),ff,abs(cPYh))
                grid
                axis([5 25 0 2*max(abs(cPXh(fi)))])
                xlabel('Frequency [Hz]')
                ylabel('Coherence sus P<>h(t)')
                legend('ITMX','ITMY')
                saveas(gcf,['./plots/Coherence_susP2h_' gps '.fig']) 

                figure(8)
                set(gcf, 'PaperSize',[8 12])
                set(gcf, 'PaperPosition', [0 0 8 12])
                set(gcf, 'Position', [0 0 600 900])
                clf
                subplot(2,1,1)
                loglog(ff,abs(trLXh),ff,abs(trLYh))
                grid
                axis([5 25 1e-14 1e-7])
                xlabel('Frequency [Hz]')
                ylabel('Magnitude sus L<>h(t) [1/m]')
                legend('ITMX','ITMY')
                subplot(2,1,2)
                semilogx(ff,angle(trLXh),ff,angle(trLYh))
                grid
                axis([5 25 -pi pi])
                xlabel('Frequency [Hz]')
                ylabel('Phase sus L<>h(t) [rad]')
                legend('ITMX','ITMY')
                saveas(gcf,['./plots/Transfer_susL2h_' gps '.fig'])  

                figure(9)
                set(gcf, 'PaperSize',[8 12])
                set(gcf, 'PaperPosition', [0 0 8 12])
                set(gcf, 'Position', [0 0 600 900])
                clf
                subplot(2,1,1)
                loglog(ff,abs(trPXh),ff,abs(trPYh))
                grid
                axis([5 25 1e-14 1e-7])
                xlabel('Frequency [Hz]')
                ylabel('Magnitude sus P<>h(t) [1/rad]')
                legend('ITMX','ITMY')
                subplot(2,1,2)
                semilogx(ff,angle(trPXh),ff,angle(trPYh))
                grid
                axis([5 25 -pi pi])
                xlabel('Frequency [Hz]')
                ylabel('Phase sus P<>h(t) [rad]')
                legend('ITMX','ITMY')
                saveas(gcf,['./plots/Transfer_susP2h_' gps '.fig'])   

                % NN coupling model
                L = 4000;       %LIGO arm length [m]
                c = 350;        %Rayleigh speed [m/s]
                gamma = 0.8;
                G = 6.678e-11;  %Gravitational constant
                rho0 = 2400;    %mass density of concrete [kg/m^3]
                h = 1.5;        %test-mass height above ground [m]
                corr = 1.1-0.11/5*ff; % comes from TiltNN.m Mathematica code

                hbytilt = 1./(L*(2*pi*ff).^2).*(c./ff)*gamma*G*rho0.*exp(-h*2*pi*ff/c).*corr;

                figure(11)
                set(gcf, 'PaperSize',[8 6])
                set(gcf, 'PaperPosition', [0 0 8 6])
                clf
                semilogx(ff,abs(ch))
                grid
                axis([5 25 0 2*max(abs(ch(fi)))])
                xlabel('Frequency [Hz]')
                ylabel('Coherence tilt<>h(t)')
                saveas(gcf,['./plots/Coherence_tilt2h_' gps '.fig'])      

                figure(12)
                set(gcf, 'PaperSize',[8 12])
                set(gcf, 'PaperPosition', [0 0 8 12])
                set(gcf, 'Position', [0 0 600 900])
                clf
                subplot(2,1,1)
                loglog(ff,abs(trh),ff,hbytilt,'r--')
                grid
                axis([5 25 1e-15 1e-8])
                xlabel('Frequency [Hz]')
                ylabel('Magnitude tilt<>h(t) [1/rad]')
                subplot(2,1,2)
                semilogx(ff,angle(trh))
                grid
                axis([5 25 -pi pi])
                xlabel('Frequency [Hz]')
                ylabel('Phase tilt<>h(t) [rad]')
                saveas(gcf,['./plots/Transfer_tilt2h_' gps '.fig'])

                close all
            end
            
            clear samples_susL samples_susP samples_tilt samples_h samples_flag
        else
            disp(['GPS: ' num2str(gps,12)])
            disp(['T*fs = ' num2str(N), ', length(samples_h) = ' num2str(length(samples_h)) ...
                ', length(samples_tilt) = ' num2str(length(samples_tilt))])
        end
    end
end

%% plot time series
% ti = 1:round(N/1000):N;
% N_flag = length(samples_flag);
% ti_flag = 1:round(N_flag/1000):N_flag;
% 
% tt = linspace(0,T,length(ti));
% tt_flag = linspace(0,T,length(ti_flag));
% 
% figure(1)
% set(gcf, 'PaperSize',[8 10])
% set(gcf, 'PaperPosition', [0 0 8 10])
% clf
% subplot(2,1,1)
% plot(tt/3600,samples_h(ti),'LineWidth',2)
% axis tight
% grid
% xlabel('Time [h]')
% ylabel('Strain noise')
% subplot(2,1,2)
% plot(tt_flag/3600,samples_flag(ti_flag),'.','MarkerSize',20,'LineWidth',2)
% axis tight
% grid
% xlabel('Time [h]')
% ylabel('DQ flag')
% set(gca,'ylim',[-0.5 7.5])
% saveas(gcf,'./plots/Timeseries_h.png')
% 
% 
% figure(2)
% set(gcf, 'PaperSize',[8 6])
% set(gcf, 'PaperPosition', [0 0 8 6])
% clf
% plot(tt/3600,samples_array(ti,:),'LineWidth',2)
% axis tight
% grid
% xlabel('Time [h]')
% ylabel('Array signals [m/s]')
% saveas(gcf,'./plots/Timeseries_array.png')
% 
% figure(3)
% set(gcf, 'PaperSize',[8 6])
% set(gcf, 'PaperPosition', [0 0 8 6])
% clf
% plot(tt/3600,samples_tilt(ti),'LineWidth',2)
% axis tight
% grid
% xlabel('Time [h]')
% ylabel('Tilt signal [rad]')
% saveas(gcf,'./plots/Timeseries_tilt.png')
% 
% figure(4)
% set(gcf, 'PaperSize',[8 6])
% set(gcf, 'PaperPosition', [0 0 8 6])
% clf
% plot(tt/3600,samples_sus(ti,:),'LineWidth',2)
% axis tight
% grid
% xlabel('Time [h]')
% ylabel('Suspension points [m]')
% legend('ITMX, L','ITMY, L')
% saveas(gcf,'./plots/Timeseries_sus.png')


%%
% figure(100)
% subplot(2,1,1)
% loglog(ff,sqrt(SS))
% axis([5 25 1e-25 1e-15])
% grid
% subplot(2,1,2)
% loglog(ff,sqrt(SS(:,ii)))
% set(gca,'xlim',[5 25])
% grid
%% spectral densities
% [S_h, ff] = pwelch(samples_h(ii_stretch(ii_select)),antialiasing,0,nfft,fs);
% 
% figure(5)
% set(gcf, 'PaperSize',[8 6])
% set(gcf, 'PaperPosition', [0 0 8 6])
% clf
% loglog(ff,sqrt(S_h),'LineWidth',2)
% grid
% set(gca,'xlim',[5 25])%,'ylim',[1e-24 1e-18])
% % set(gca,'ytick',10.^(-24:-18))
% xlabel('Frequency [Hz]')
% ylabel('Strain noise [1/\surdHz]')
% saveas(gcf,'./plots/Spectrum_strain.png')
% 
% S_array = zeros(nfft/2+1,ns);
% for k = 1:ns
%     S_array(:,k) = pwelch(samples_array(ii_stretch(ii_select),k),antialiasing,0,nfft,fs);
% end
% 
% 
% figure(6)
% set(gcf, 'PaperSize',[8 6])
% set(gcf, 'PaperPosition', [0 0 8 6])
% clf
% loglog(ff,sqrt(S_array),'LineWidth',2)
% grid
% set(gca,'xlim',[1 25])%,'ylim',[1e-3 1])
% % set(gca,'ytick',10.^(-3:0))
% xlabel('Frequency [Hz]')
% ylabel('Ground vertical [(m/s)/\surdHz]')
% saveas(gcf,'./plots/Spectra_array.png')
% 
% [S_tilt, ff] = pwelch(samples_tilt(ii_stretch(ii_select)),antialiasing,0,nfft,fs);
% 
% figure(7)
% set(gcf, 'PaperSize',[8 6])
% set(gcf, 'PaperPosition', [0 0 8 6])
% clf
% loglog(ff,sqrt(S_tilt),'LineWidth',2)
% grid
% set(gca,'xlim',[10 25])%,'ylim',[1e-24 1e-18])
% % set(gca,'ytick',10.^(-24:-18))
% xlabel('Frequency [Hz]')
% ylabel('Ground tilt [rad/\surdHz]')
% saveas(gcf,'./plots/Spectrum_tilt.png')
% 
% S_sus = zeros(nfft/2+1,2);
% for k = 1:2
%     S_sus(:,k) = pwelch(samples_sus(ii_stretch(ii_select),k),antialiasing,0,nfft,fs);
% end
% 
% 
% figure(8)
% set(gcf, 'PaperSize',[8 6])
% set(gcf, 'PaperPosition', [0 0 8 6])
% clf
% loglog(ff,sqrt(S_sus),'LineWidth',2)
% grid
% axis([5 25 1e-11 1e-10])
% xlabel('Frequency [Hz]')
% ylabel('Suspension point [m/\surdHz]')
% saveas(gcf,'./plots/Spectra_sus.png')


