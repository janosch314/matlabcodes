function XY = coords_map()
% Return the coordinates of the 2016 array sensors in METERS

% David McManus 2016, NN array map
X=[];
Y=[];
% The measurements listed below are in inches, and are defined using the 
% axis directions as the X and Y arms of the interferometer, except with an
% offset so that the x=0 and y=0 axis lines run along the boundary between 
% the linoleum and the concrete inside the beer garden. These lines are 
% parallel to the arms   

% The positive direction for both axes is towards the end station.

% This places the 'origin' inside one of the Beam splitter HEPI pillars on 
% the inside of the beer garden where these lines meet, which you can look 
% at below by uncommenting the line which plots the origin on the map.

% Measurements below are in inches using the coord frame described above. 
% The vectors X and Y contain the x coords and y coords respectively. The
% index numbers match the sensor channel numbers so X(1) is the x coord of
% the sensor in channel 1 etc.

% ITM locations (measured to the approximate centre of the BSC tank)
ITMX_x=119;
ITMX_y=-62;

ITMY_x=-62;
ITMY_y=119;

% L4Cs
X(1)= -175.5;
Y(1)= -177;

X(2)= -128.5;
Y(2)=-122.5;

X(3)=18.5;
Y(3)=193.5;

X(4)=20.5;
Y(4)=56.5;

X(5)=46;
Y(5)=-11.5;

X(6)=90;
Y(6)=100;

X(7)=199.5;
Y(7)=-3.5;

X(8)=-34;
Y(8)=-37;

X(9)=-55.5;
Y(9)=126;

X(10)=544.5;
Y(10)=141;

X(11)=-206.5;
Y(11)=284;

X(12)=-168.5;
Y(12)=165;

X(13)=-128;
Y(13)=193.5;

X(14)=-352.5;
Y(14)=215.5;

X(15)=-245.5;
Y(15)=22.5;

X(16)=-132;
Y(16)=34.75;

X(17)=38;
Y(17)=486;

X(18)=100;
Y(18)=257;

X(19)=159.5;
Y(19)=142.5;

X(20)=272;
Y(20)=69;

X(21)=274;
Y(21)=-255.5;

X(22)=43.5;
Y(22)=-110;

X(23)=132.5;
Y(23)=-220.5;

X(24)=22.5;
Y(24)=-257.5;

X(25)=163;
Y(25)=-345;

X(26)=427.5;
Y(26)=-208.5;

X(27)=152.5;
Y(27)=-51;

X(28)=201.5;
Y(28)=-106.5;

X(29)=303.5;
Y(29)=-49;

X(30)=-55.5;
Y(30)=278;

%%  Convert to meters, now that figure-making is done

XY = [X' Y'] * 0.0254;




